import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Home from './components/home-cat/index';
import UploadCat from './components/upload-cat';
import { Wrapper, CatLink} from './styles/global.styles';

function App() {
  return (
    <Router>
      <Wrapper>
        <nav>
          <ul>
            <CatLink primary>
              <Link to="/">Home</Link>
            </CatLink>
            <CatLink primary>
              <Link to="/upload">Upload</Link>
            </CatLink>
          </ul>
        </nav>

        <Switch>
          <Route path="/upload">
            <UploadCat />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Wrapper>
    </Router>
  )
}
export default App;
