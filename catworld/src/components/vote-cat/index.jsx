import React, {useState} from 'react';
import axios from 'axios';
import { API_KEY } from '../index';

const VoteCat = (id) => {
    //const [voteValue, setVoteValue] = useState(0);

    const voteUpData = {
        "image_id": id.toString(),
        "sub_id": "mrugeshcat411",
        "value": 1
    }
    const voteDownData = {
        "image_id": id.toString(),
        "sub_id": "mrugeshcat411",
        "value": 0
    }

    const voteUp = async (id) => {
        console.log("voteUpData : ", voteUpData);
        const URL = `https://api.thecatapi.com/v1/votes`;
        await axios.post(URL, voteUpData, {
                headers: {
                    'Content-Type' : 'application/json',
                    'x-api-key': API_KEY,
                }
            })
            .then((res) => {
                console.log("VOTE RES DATA >> ", res);
              })
              .catch((error) => {
                console.error('VOTE ERROR >> ', error)
              })
    };

    const voteDown = async (id) => {
        console.log("Vote down", voteDownData);
        const URL = `https://api.thecatapi.com/v1/votes`;

        await axios.post(URL, voteDownData, {
                headers: {
                    'Content-Type' : 'application/json',
                    'x-api-key': API_KEY,
                }
            })
            .then((res) => {
                console.log("VOTE RES DATA >> ", res.data);
              })
              .catch((error) => {
                console.error('VOTE ERROR >> ', error)
              })
    };

    return (
        <div>
            <button onClick={() => voteUp(id)}>Vote up</button>
            <button onClick={() => voteDown(id)}>Vote down</button>
        </div>
    )
}
export default  VoteCat;