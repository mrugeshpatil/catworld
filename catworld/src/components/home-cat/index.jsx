import React from 'react'
import ListingCats from '../listing-cat';
import { Title } from '../../styles/global.styles';

const Home = () => {
    return (
        <div>
            <Title>Cat World | Home</Title>
            <ListingCats />
        </div>
    )
}
export default Home;
