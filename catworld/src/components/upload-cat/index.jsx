import React, {useState} from 'react';
import axios from 'axios';
import { useHistory } from "react-router-dom";
import { API_KEY } from '../index';
import { Title } from '../../styles/global.styles';

 const UploadCat = () => {
    const [imageFile, setimageFile] = useState(null);
    const [error, setError] = useState(null);
    const history = useHistory();

    const imageSelectHandler = event => {
        setimageFile(event.target.files[0]);
    }

    const imageUploadHandler = async () => {
        const formData = new FormData();
        formData.append("file", imageFile);
        formData.append("sub_id", 'mrugeshcat411');
          
        await axios.post('https://api.thecatapi.com/v1/images/upload', formData, {
            headers: {
                'Content-Type' : 'multipart/form-data',
                'x-api-key': API_KEY
            }
        })
        .then((res) => {
            history.push('/');
          })
          .catch((error) => {
            setError(error.message);
            console.error('ERROR >> ', error.message)
          }) 
     }

    return (
        <div>
            <Title>Cat World | Upload</Title>
           <input type="file" onChange={imageSelectHandler}/>
           <button onClick={imageUploadHandler}>Upload Cat Image</button>
           {
               error && <div>
                   <h3>Problem uploading image</h3>
                   <p>{error}</p>
               </div>
           }
        </div>
    )
}
export default UploadCat;