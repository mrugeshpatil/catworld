import styled from "styled-components";

export const FavButton = styled.button`
    display: inline-block;
    position: absolute;
    right: 20px;
    top: 20px;
    font-size: 16px;
    cursor: pointer;
    
  &::before {
    font-size: 3em;
    color: #000;
    content: '♥';
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
  }
  &::after {
    font-size: 3em;
    color: palevioletred;
    content: '♥';
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%) scale(0);
    transition: transform 0.2s;
  }
  &.liked::after {
    transform: translate(-50%, -50%) scale(1.1);
  }
`;