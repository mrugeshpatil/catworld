import React, { useState } from 'react';
import axios from 'axios';
import { API_KEY } from '../index';
import { FavButton } from './fav-unfav-styles';

const FavUnfavCat = ({id, favourite}) => {
    const [unFavCatId, setUnFavCatId ] = useState(favourite ? favourite.id : null);
    const [ isFav, setIsFav ] = useState(favourite ? true : false);

    const favUnfavHandler = async (id) => {

        const favUnfavData = {
            "image_id": id,
            "sub_id": "mrugeshcat411",
        }

        if(!isFav) {            
            await axios.post(`https://api.thecatapi.com/v1/favourites`, favUnfavData, {
                headers: {
                    'Content-Type' : 'application/json',
                    'x-api-key': API_KEY,
                }
            })
            .then((res) => {
               setUnFavCatId(res.data.id);
               setIsFav(true);
              })
              .catch((error) => {
                console.error('FAV ERROR >> ', error)
              })
              
         }
         else {           
            const URL = `https://api.thecatapi.com/v1/favourites/${unFavCatId}`;

            await axios.delete(URL, {
                headers: {
                    'x-api-key': API_KEY,
                },
            })
            .then((res) => {
               setIsFav(false);
              })
              .catch((error) => {
                console.error('FAV ERROR >> ', error)
              })
        }
    };

    return (        
    <FavButton 
        onClick={() => favUnfavHandler(id)}
        className={isFav ? 'liked' : null}
    >
    </FavButton>        
    )
}
export default FavUnfavCat;
