import styled from "styled-components";

export const ListWrapper = styled.ul`
    list-style: none;
    margin: 0;
    display: flex;
    flex-wrap: wrap;
    padding: 5px;
`;

export const GalleryItem = styled.li`
  margin: 5px;
  border: 1px solid #ccc;
  width: 180px;
position: relative;
    img {
        width: 100%;
        height: auto;   
    } 

`;