import React, {useState, useEffect} from 'react'
import axios from 'axios';
import { API_KEY } from '../index';
import FavUnfavCat from '../fav-unfav-cat';
import VoteCat from '../vote-cat';

import { ListWrapper, GalleryItem } from './listing-styles';

const ListingCats = () => {
    
    const [ uploadedImageData, setUploadedImageData ] = useState([]);

    const catListingHandler = async () => {
        console.log('=========> catListingHandler');
        await axios.get('https://api.thecatapi.com/v1/images?sub_id=mrugeshcat411&limit=50', {
            headers: {
                'x-api-key': API_KEY
              }
        })
        .then((response) => {
            setUploadedImageData(response.data);
            console.log("LIST DATA >> ", response.data);
          })
          .catch((error) => {
            console.log("=====> ERROR >> ", error);
          });
    };

    useEffect( () => {
        catListingHandler();
    },[]);

    return (
        <div>
            <h3>Listing Cats</h3>
            <ListWrapper>
            {
               uploadedImageData && uploadedImageData.map( image =>                
                <GalleryItem key={image.id}>
                    <img 
                        src={image.url} 
                        alt={image.original_filename}
                        width={200}
                        height={200}
                    />
                    <FavUnfavCat id={image.id} favourite={image.favourite} />
                    <VoteCat id={image.id} />                                
                </GalleryItem>)
            }
             </ListWrapper>
        </div>
    )
}
export default ListingCats;